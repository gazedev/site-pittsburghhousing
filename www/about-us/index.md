---
layout: page
title: About Us
---
# About Pittsburgh Housing

Pittsburgh Housing is a project powered by HousingDB which is fiscally sponsored through Open Collective. Both Pittsburgh Housing and HousingDB are projects of [Gaze, LLC](https://gaze.dev), a Pittsburgh-based web development company. You can donate to support the project on [our Open Collective page](https://opencollective.com/housingdb)

# Our Mission & Values

The mission of Pittsburgh Housing is to empower people by providing resources, data, and tools that promote clean, safe, affordable, high-quality rental housing across Allegheny County.

Pittsburgh Housing is driven by eight **values**:
- **Follow a Social Good Motivation:** We’re always going to strive for doing the right thing in everything we do.
- **Empower All:** We want to make sure that everyone, including tenants, landlords, and community members, have a voice at the table and the tools they need to make informed decisions. This empowerment really is for all, including those who do not speak English, who are in poverty, have disabilities, are LGBT+, have mental illness, or are in other disadvantaged groups.
- **Work Towards High Quality and Precision:** We want to make sure that when we do something, we do it well. We want to make our tools easy to use and accurate, and include the people who will be using them in the development process.
- **Be Community Run:** Pittsburgh Housing is run by local people for local people. We are focused on local resources and data and strive for partnerships with local organizations to make sure that we are doing right by Pittsburgh.  
- **Be Trustworthy:** We will always work with integrity, including putting user privacy and security as a top priority.  
- **Be Open Source:** More than making our code public, open source means that we have an emphasis on collaboration, sharing, and transparency; because we believe that approaching challenges in this way makes solutions better.
- **Make Replicable Solutions:** We know that a good solution can go further if it can be repeated in other places. We strive to make our problem solving something that we can all benefit from, not just people in one area.
- **Always Seek to Improve:** We recognize that we always have room to get better, so we will always strive to grow and improve. 
