---
layout: page
title: Join Our Facebook Group
---

# Join the Pittsburgh Housing & Roommates Facebook Group

The Pittsburgh Housing and Roommates Facebook group is one of the best groups in the area.

Use this code when joining:

<div class="copy-url" onfocusout="tooltipUnfocus()">
  <input id="copy-box" type="text" value="c85c38c9-802e-4b3d-86a7-c4ffb1900b10" readonly>
  <div class="copy-url-interactive">
    <button type="button" onclick="copyLink()">Copy Code</button>
    <span class="tooltip-text" id="tooltip-text">Copy to clipboard</span>
  </div>
  <noscript>
    <p>Select and copy the text and send it to someone you know.</p>
    <style>
      .copy-url-interactive {
        display: none;
      }
    </style>
  </noscript>
</div>
<script>
  function copyLink() {
    var copyText = document.getElementById("copy-box");
    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/
    /* Copy the text inside the text field */
    document.execCommand("copy");
    var tooltip = document.getElementById("tooltip-text");
    tooltip.innerText = "Copied!";
  }
  function tooltipUnfocus() {
    var tooltip = document.getElementById("tooltip-text");
    tooltip.innerHTML = "Copy to clipboard";
  }
</script>

Request to join our group on Facebook: [Pittsburgh Housing and Roommates](https://www.facebook.com/groups/PittsburghHousing)


We take an active role in moderating the group. We moderate the join requests, regularly remove content that doesn't belong, and we have a zero tolerance policy for harassment. In addition, we go above and beyond and actively investigate scams and warn our members when we find them.

We have seen an increase in spam/scam requests to join the group. As such, we are implementing new tools to help guard our group and our members from spam and scams.

Make sure to copy the code above and paste it into the join request. This will help us identify you as a real person and not a spammer/scammer.