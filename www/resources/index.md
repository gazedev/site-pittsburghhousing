---
layout: page
title: Resources
---

# Resources

Also check out [CoViD-19 Resources](/covid-19)

On this page we'll be adding resources for a variety housing and housing related services, tools, organizations, etc.

[Deduct &amp; Repair - Tenant's Right to a Safe and Decent Home](https://www.palawhelp.org/resource/deduct-repair-tenants-right-to-a-safe-and-dec) [Repairs]

[Basics of a defamation claim in Pennsylvania](http://www.timesonline.com/44603520-f45b-11e6-8ea1-830614246374.html) [Reviews]

[Fair Housing Act coverage of Emotional Support Animals (pdf)](https://archives.hud.gov/news/2013/servanimals_ntcfheo2013-01.pdf) [Animals]

## Places Pittsburgh Housing Finds Data

We use a number of resources when we are trying to compile data about properties, landlords, and reviews. Much of it is a lot of searching and cross referencing information from different sources, which we then make simple for you by collecting, organizing, formatting, and linking it. We haven't researched all properties and landlords though, so here are resources that you yourself can use to do some investigative work. If you find something we don't have, feel free to let us know via our email address, [gazepgh+resources@gmail.com](mailto:gazepgh+resources@gmail.com), which you can also use to ask us to research a property for you.

### WPRDC Property Dashboard [Research]:

The Western PA Regional Data Center has a property dashboard that aggregates data from several sources about a property. You can look up a property you are interested in by address, view building code violations, see the property owner, and highlight other properties with the same owner information.

- [Go to the WPRDC Property Dashboard](http://tools.wprdc.org/property-dashboard/)

### Misc Places to Look for Reviews [Reviews]:

Reviews of properties and landlords can come from all over the internet, from other review sites, to forums, and even from sites that no longer exist (thanks to the [Internet Archive Wayback Machine](https://web.archive.org/)). When we do research we will save the links to other reviews, which when found are listed in the 'External Reviews' section of each landlord and property page. Here are some of the places we link to reviews on:<br>
- Google.com
- Reddit.com
- Yelp.com
- Reviewmylandlord.com
- Whoseyourlandlord.com

## Other City Data Resources:

[BurghsEyeView](https://pittsburghpa.shinyapps.io/BurghsEyeView/) [Crime, Permits, Citations]:<br>

[Wikipedia Pittsburgh Neighborhoods Map](https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Pittsburgh_Pennsylvania_neighborhoods_fade.svg/1500px-Pittsburgh_Pennsylvania_neighborhoods_fade.svg.png)


## Landlord Responsibilities:

### Waste and Recycling:

[Updated Waste and Recycling Legislation (pdf)](http://apps.pittsburghpa.gov/redtail/images/5111_Chapter_619_Legislation.pdf)

[Responsibilities and Waste Disposal and Recycling Fact Sheet (pdf)](http://apps.pittsburghpa.gov/dpw/Responsibilities_of_Landlord_and_Tenant.pdf)
