# Node v12
FROM node:12.18.2

# Create app directory
WORKDIR /usr/src/app

# For npm@5 or later, copy package-lock.json as well
COPY package.json package-lock.json ./

RUN npm install

RUN npm install --save-dev @11ty/eleventy

# Bundle app source
COPY . .

EXPOSE 8100
EXPOSE 35729
