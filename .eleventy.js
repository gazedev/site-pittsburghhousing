module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('www/assets');
  eleventyConfig.addPassthroughCopy('www/css');
  eleventyConfig.addPassthroughCopy('www/_data/appMenu.json');
  eleventyConfig.addPassthroughCopy('www/_redirects');

  return {
    passthroughFileCopy: true,
    dir: {
      input: "www",
      layouts: "/_layouts",
    }
  }
}
